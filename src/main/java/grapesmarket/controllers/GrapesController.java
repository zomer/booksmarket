package grapesmarket.controllers;

import grapesmarket.jpaImp.GrapesRepository;
import grapesmarket.jpaImp.GrapesService;
import grapesmarket.models.Grapes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * Created by zzz on 08.04.15.
 */
@Controller
@RequestMapping("/admin/grapes")
public class GrapesController {

    GrapesService grapesService;

    @Autowired
    public GrapesController(@Qualifier("grapesService") GrapesService grapesService){
        this.grapesService = grapesService;
    }

    @RequestMapping(value="/add", method=RequestMethod.GET)
    public String add(@ModelAttribute Grapes grapes) {
        return "backend/grapes/add";
    }

    @RequestMapping(value="/doAdd", method=RequestMethod.POST)
    public String doAdd(@Valid @ModelAttribute Grapes grapes, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "backend/grapes/add";
        }
        grapesService.add(grapes);
        return "redirect:/admin/grapes/list";
    }

    @RequestMapping(value="/list", method=RequestMethod.GET)
    public ModelAndView list(){
        ModelAndView mav = new ModelAndView("backend/grapes/list");
        mav.addObject("grapesList", grapesService.findAll());
        return mav;
    }

    @RequestMapping(value="/edit", method=RequestMethod.GET)
    public String edit(@RequestParam("id") Long id, Model model) {
        Grapes grapes = grapesService.findById(id);
        if (grapes != null) {
            model.addAttribute("grapes", grapes);
        }
        return "backend/grapes/edit";
    }

    @RequestMapping(value="/doEdit", method=RequestMethod.POST)
    public String doEdit(@Valid @ModelAttribute Grapes grapes, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "backend/grapes/edit";
        }
        grapesService.edit(grapes);
        return "redirect:/admin/grapes/list";
    }

    @RequestMapping(value="/delete", method={RequestMethod.GET, RequestMethod.POST})
    public String delete(@RequestParam("id") Long id) {
        Grapes grapes = grapesService.findById(id);
        if (grapes != null) {
            grapesService.delete(grapes);
        }
        return "redirect:/admin/grapes/list";
    }
}
