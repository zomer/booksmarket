package grapesmarket.controllers;

import grapesmarket.jpaImp.GrapesRepository;
import grapesmarket.models.Grapes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Map;

/**
 * Created by zzz on 15.04.15.
 */
@Controller
public class AdminController {

    @Autowired
    private GrapesRepository grapesRepository;

    public ModelAndView index() {
        ModelAndView mav = new ModelAndView("backend/index");
        List<Grapes> grapes = grapesRepository.findAll();
        mav.addObject("grapes", grapes);
        return mav;
    }

}
