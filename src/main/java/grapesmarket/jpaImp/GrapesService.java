package grapesmarket.jpaImp;

import grapesmarket.models.Grapes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

/**
 * Created by zzz on 21.04.15.
 */
@Transactional
@Service("grapesService")
public class GrapesService {

    @Autowired
    private GrapesRepository grapesRepository;

    public Collection<Grapes> findByTitle(String title) {
        return grapesRepository.findByTitle(title);
    }

    public void add(Grapes grapes) {
        grapesRepository.saveAndFlush(grapes);
    }

    public void edit(Grapes grapes) {
        grapesRepository.saveAndFlush(grapes);
    }

    public void delete(Grapes grapes) {
        grapesRepository.delete(grapes);
    }

    public List<Grapes> findAll() {
        return grapesRepository.findAll();
    }

    public Grapes findById(Long id) {
        return grapesRepository.findOne(id);
    }
}
