package grapesmarket.jpaImp;

import grapesmarket.models.Grapes;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

/**
 * Created by zzz on 08.04.15.
 */
public interface GrapesRepository extends JpaRepository<Grapes, Long> {
    Collection<Grapes> findByTitle(String title);
}
