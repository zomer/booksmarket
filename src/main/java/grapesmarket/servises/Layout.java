package grapesmarket.servises;

import java.lang.annotation.*;

/**
 * Created by zzz on 20.04.15.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Layout {
    String value() default "";
}